PROJECT := cv-prototype-using-make

# List of python versions for building libraries.
PYTHON_VERSIONS := 3.9.6

# List of libraries to be built.
LIBRARY_COMPONENTS := messaging

# TODO: maybe create separate config for each network
ifeq ($(NETWORK),U)
#DOCKER_REGISTRY := nexus.gs.mil  # TODO: uncomment
ARGO_CD_REPO := git@gitlab.com:michael.john.albers/argo-cd.git
# TODO: restore
#SAFFIRE_PIP_REPO_URL := https://nexus.gs.mil/saffire/pip/simple/
SAFFIRE_PIP_REPO_URL := https://test.pypi.org/legacy/
else ifeq ($(NETWORK),S)
DOCKER_REGISTRY := nexus.gs.smil.mil
ARGO_CD_REPO := ???
SAFFIRE_PIP_REPO_URL := https://nexus.gs.smil.mil/saffire/pip/simple/
else ifeq ($(NETWORK),TS)
DOCKER_REGISTRY := nexus.nga.ic.gov
ARGO_CD_REPO := ???
SAFFIRE_PIP_REPO_URL := https://nexus.gs.ic.gov/saffire/pip/simple/
else
$(error Unknown 'NETWORK' value, '$(NETWORK)'. Must be one of 'U', 'S' or 'TS')
endif

ifdef CI_COMMIT_SHA
## Running in Gitlab CI
DOCKER_TAG := $(CI_COMMIT_SHORT_SHA)
ALLOW_ARGO_CD_UPDATE := yes
else
## Developer build
# Modify the path so we don't get dev images mixed in with the official images.
PROJECT := $(addsuffix /$(USER), $(PROJECT))
DOCKER_SCAN := trivy image
DOCKER_SCAN_ARGS := --severity CRITICAL,HIGH
ALLOW_ARGO_UPDATE := no
endif
