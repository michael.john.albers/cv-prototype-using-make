# This file contains rules for updating Argo CD app definitions.
# All rules are included for free in the top-level Makefile.

# This is something developers shouldn't be doing as a matter of course, so
# simply don't create the rules. But do allow an out, in case.
ifneq ($(filter yes,$(ALLOW_ARGO_CD_UPDATE) $(ALLOW_ARGO_CD_UPDATE_FORCE)),)

DOCKER_PUSHED_STATEFILES := $(addsuffix /Dockerfile.pushed, $(addprefix .build/,$(DOCKER_COMPONENTS)))

.PHONY: argo-cd.update-apps
argo-cd.update-apps:: .build/argo-cd.update-apps
.build/argo-cd.update-apps: $(DOCKER_PUSHED_STATEFILES)
	@.mkplugins/update-argo-cd/update-argo-cd-wrapper.sh $(ARGO_CD_REPO) $(DOCKER_TAG) $(patsubst .build/%/,%,$(dir $?))
	@touch .build/argo-cd.update-apps

clean::
	@rm -rf .build/argo-cd.update-apps

endif
