.PHONY: docker.login
docker.login:
	@echo "$(SAFFIRE_DOCKER_REGISTRY_PASSWORD)" | docker login "$(DOCKER_REGISTRY)" --username "$(SAFFIRE_DOCKER_REGISTRY_USERNAME)" --password-stdin
