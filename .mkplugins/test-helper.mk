# This file contains functions used to help add unit tests for directories with Dockerfiles.
# The main Makefile delegates that to each component. Most of the time this will be lots
# of boilerplate code. Hence these functions - to reduce that.
#
# Tests for Poetry libraries are found in the poetry-libraries.mk file.

###
### Generic function used to generate rules to run Poetry tests within a Docker container.
### assumes the use of Poetry (installed by root) and Pytest
### To use it, add this code
###   include .mkplugins/test-helper.mk
###   $(eval $(call gen-poetry-test-in-container,<component name here>))
### Arguments:
### $1 component name
define gen-poetry-test-in-container
$1.tests.unit:: .build/$1/tests.unit.run
# The junit XML is generated for Gitlab CI.
.build/$1/tests.unit.run: $(shell find $1/$1 $1/tests -name '*.py') $1/poetry.lock $1/pyproject.toml $1/Dockerfile
	@mkdir -p .build/$1
	@pushd $1 >/dev/null && docker build . -t $1:test-image && popd > /dev/null
	@docker run --rm \
	 --entrypoint poetry \
	 --volume $(shell pwd)/$(1):/mnt/ \
	 $1:test-image run pytest --junitxml=/mnt/report.xml
	@touch .build/$1/tests.unit.run

$1.clean::
	@rm -rf $1/report.xml

endef
