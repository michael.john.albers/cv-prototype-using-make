#!/usr/bin/env python3

import os
import subprocess
import sys
import tempfile
import yaml

argocd_repo = sys.argv[1]
image_tag = sys.argv[2]
app_list = sys.argv[3:]

# TODO: need to use project access token for clone (switch repo to use HTTPS instead of SSH)
with tempfile.TemporaryDirectory(prefix="cv-build-argo-cd") as temp_dir:
    os.chdir(temp_dir)
    subprocess.run(["git", "clone", argocd_repo])
    repo_dir = os.path.basename(argocd_repo).replace(".git", "")
    os.chdir(repo_dir)
    for app in app_list:
        with open(f'templates/{app}.yaml', "r+") as yaml_file:
            app_yaml = yaml.safe_load(yaml_file)
            for parameter in app_yaml['spec']['source']['helm']['parameters']:
                if parameter['name'] == 'image.tag':
                    parameter['value'] = image_tag
            yaml_file.seek(0)
            yaml.dump(app_yaml, yaml_file, default_flow_style=False)
            yaml_file.truncate()
    app_string = ",".join(app_list)
    subprocess.run(["git", "commit", "-a", "-m",
                    f"Set image tag for apps {app_string} to '{image_tag}' by build process."])
    subprocess.run(["git", "push"])
