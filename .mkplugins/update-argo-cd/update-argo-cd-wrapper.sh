#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

tempDir=$(mktemp --directory --tmpdir=/tmp update-argo-cd-XXXXXX)

function finish {
  /bin/rm -rf $tempDir
}
trap finish EXIT

cd $tempDir
python3 -m venv venv
source venv/bin/activate
pip3 install --upgrade pip  # Not necessary, but I hate the pip upgrade warning
pip3 install -r $SCRIPT_DIR/requirements.txt
$SCRIPT_DIR/update-argo-cd.py "$@"
