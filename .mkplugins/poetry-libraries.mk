# This file adds rules for building, testing, publishing and cleaning Poetry-based libraries.
# All rules are included for free in the top-level Makefile.

.PHONY: libs libs.tests libs.tests.unit libs.tests.integration tests.unit tests.integration clean

libs:: $(addsuffix .build,$(LIBRARY_COMPONENTS))

libs.build:: $(addsuffix .build,$(LIBRARY_COMPONENTS))

libs.tests:: $(addsuffix .tests,$(LIBRARY_COMPONENTS))
libs.tests.unit:: $(addsuffix .tests.unit,$(LIBRARY_COMPONENTS))
libs.tests.integration:: $(addsuffix .tests.integration,$(LIBRARY_COMPONENTS))

libs.publish:: $(addsuffix .publish,$(LIBRARY_COMPONENTS))

tests.unit:: $(addsuffix .tests.unit,$(LIBRARY_COMPONENTS))
tests.integration:: $(addsuffix .tests.integration,$(LIBRARY_COMPONENTS))

clean:: $(addsuffix .clean,$(LIBRARY_COMPONENTS))

### Arguments
### $1: library component name
define make-library-rules

.PHONY: $1.build $1.tests $1.tests.unit $1.tests.integration $1.publish $1.clean

# Cannot use the 'poetry' executable here as some of the Gitlab CI jobs don't
# have poetry installed and it causes errors (non-fatal, but better to just)
# avoid them.
$1_WHEEL_NAME := $(shell cd $1 && egrep '^name\s*=' pyproject.toml | sed -e 's/^name\s*=\s*"\(.*\)"/\1/')
$1_WHEEL_VERSION := $(shell cd $1 && egrep '^version\s*=' pyproject.toml | sed -e 's/^version\s*=\s*"\(.*\)"/\1/')

$1/dist/$${$1_WHEEL_NAME}-$${$1_WHEEL_VERSION}-py3-*.whl: $(shell find $1/$1 -name '*.py'  | sed 's/ /\\ /g') $1/poetry.lock $1/pyproject.toml
	@cd $1 && poetry build --format wheel
$1.build: $1/dist/$${$1_WHEEL_NAME}-$${$1_WHEEL_VERSION}-py3-*.whl

$1.tests:: $1.tests.unit $1.tests.integration

$1.tests.unit:: .build/$1/tests.unit.run
# The junit XML is generated for Gitlab CI.
.build/$1/tests.unit.run: $(shell find $1/$1 $1/tests -name '*.py'  | sed 's/ /\\ /g') $1/poetry.lock $1/pyproject.toml
	@mkdir -p .build/$1
	@pushd $1 >/dev/null ; poetry install && poetry run pytest --junitxml=report.xml ; popd >/dev/null
	@touch .build/$1/tests.unit.run

$1.tests.integration::

$1.publish:: .build/$1/library.published
.build/$1/library.published: .build/$1/tests.unit.run
	@pushd $1 >/dev/null && \
	 poetry config repositories.saffire-repo "$(SAFFIRE_PIP_REPO_URL)" && \
	 poetry publish -r saffire-repo && \
	 popd >/dev/null
	@touch .build/$1/library.published

$1.clean::
	@rm -rf .build/$1
	@rm -rf $1/dist $1/.pytest_cache
	@rm -f $1/report.xml

endef

$(foreach component,$(LIBRARY_COMPONENTS),$(eval $(call make-library-rules,$(component))))
