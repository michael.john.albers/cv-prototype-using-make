# Cv Prototype

# Requirements
You'll need to make sure the following are installed on your system on can be found on your `$PATH`.
* [GNU Make](https://www.gnu.org/software/make/), 3.81 or later
* [Python](https://www.python.org), multiple versions, probably
* [Poetry](https://python-poetry.org), probably latest version
* [Docker](https://www.docker.com), latest version
* [Trivy](https://github.com/aquasecurity/trivy), latest version, only needed for `scan` targets run by devs.

## Building
This project uses `make` to build though each microservice can be built individually with plain
`poetry` and `docker` commands.

The top level **Makefile** is take from [here](https://github.com/enspirit/makefile-for-monorepos).

## Libraries
When adding a new library, add it to the library list in config.mk. I don't
know of a good way to automatically detect if a project is a library which
needs to be published.

Libraries are expected to use Poetry and Pytest.

Libraries are published to a PyPi compatible repo and then used like any other
library by other microservices. With `poetry` we could probably install the
library as a relative path, but then this would require sending the *entire*
repository directory to the Docker daemon when building.
