# This jobs in this pipeline will only run on changes to the main branch (i.e.,
# merged MRs) or on the creation of MRs (in order to test changes before a
# merge, to hopefully catch any bugs then).
# TODO: this will probably cause issues with MR builds: services will be updated to new
#       library version, but it won't have been published yet.
# TODO: how to run pipeline on hotfix branches? But don't update Argo CD for hotfixes
#       (that will be manual initially, but we should work towards automating it)
# TODO: use case tests
#       - push directly to master, should run all jobs
#       - create MR, should run all jobs except publish/push/deploy
#       - add commit to MR, should run all jobs except publish/push/deploy
#       - create branch, should not run at all

# TODO: probably need special CA certs for high side (docker pull, pip installs, etc.)

# Rules to only run a job on an MR to master or changes to master
# (direct commit or MR merge)
.rules-default: &rules-default
  rules:
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH'
      when: always
    - when: never

# Rules to only run a job on changes to master
.rules-main-only: &rules-main-only
  rules:
    - if:
      when: always
    - when: never

.install-poetry: &install-poetry
  - python3 -m pip install $PIP_ARGS --user pipx
  - python3 -m pipx ensurepath
  - PATH="${PATH}:~/.local/bin"
  - pipx install --pip-args="$PIP_ARGS" "poetry==$POETRY_VERSION"

# Git doesn't track file times. When a repo is cloned, all the file timestamps
# are set to the current time. This causes problems with `make` as all of the
# dependency files will ALWAYS be older than the cloned files. This little
# script attempts to set the modified times so that `make` can properly build
# only what it needs to (instead of everything).
.restore-mtime: &restore-mtime
  - export PATH=${PATH}:.mkplugins/git-restore-mtime
  - git restore-mtime

# TODO: need to set SAFFIRE_DOCKER_REGISTRY_PASSWORD and SAFFIRE_DOCKER_REGISTRY_USERNAME as project variables

variables:
  BUILD_TYPE: production

  # TODO: set this via a project variable so it can vary between networks
  NETWORK: U

  # TODO: set these via project variable (need --index-url, --trusted-host)
  #       On TS use ADV, on S use ???, on U leave empty
  PIP_ARGS: ""

  POETRY_VERSION: 1.1.8
  POETRY_VIRTUALENVS_PATH: $CI_PROJECT_DIR/virtualenvs

# This cache is critical to the Makefiles. All the state is stored in
# .build. That information keeps targets from rebuilding unnecessarily.
# Every job in the pipeline shares the same cache. But CI_COMMIT_REF_SLUG
# ensures different branches use different caches. So each MR has its own
# cache, all of which are different from the main branch cache.
# TODO: on MRs on the first build, this will build EVERYTHING
cache:
  key: $CI_COMMIT_REF_SLUG
  paths:
    - .build/
    # Only used for libraries, microservices are built via Docker so this has no effect for them.
    - virtualenvs/
    # For libraries, the wheels are created in these directories
    - '*/dist/'

stages:
  - lib-build
  - lib-test
  - lib-publish
  - microservice-test
  - microservice-build-push
# TODO: remove
#  - microservice-push
  - microservice-scan
  - deploy

build-libraries:
  <<: *rules-default
  stage: lib-build
  image: python:3.9.6-buster
  before_script:
    - *install-poetry
    - *restore-mtime
  script:
    - make libs.build

test-libraries:
  <<: *rules-default
  stage: lib-test
  image: python:3.9.6-buster
  before_script:
    - *install-poetry
    - *restore-mtime
  script:
    - make libs.tests
  artifacts:
    when: always
    reports:
      junit: '*/report.xml'

publish-libraries:
  <<: *rules-main-only
  stage: lib-publish
  image: python:3.9.6-buster
  variables:
    # TODO: these need to be set via project variables (for secrecy)
    POETRY_HTTP_BASIC_SAFFIRE_REPO_USERNAME: $SAFFIRE_PIP_REPO_USERNAME
    POETRY_HTTP_BASIC_SAFFIRE_REPO_PASSWORD: $SAFFIRE_PIP_REPO_PASSWORD
  before_script:
    - *install-poetry
    - *restore-mtime
  script:
    - make libs.publish

# TODO: for high side, we'll build our own versions of the docker and docker-dind images, and install the packages
# we need (see before_script below) then somehow upscan them.

test-microservices:
  <<: *rules-default
  stage: microservice-test
  image: docker:20.10.8
  services:
    - docker:20.10.8-dind
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
  before_script:
    - apk add make bash git python3
    - *restore-mtime
  script:
    - make tests
  artifacts:
    when: always
    reports:
      junit: '*/report.xml'

build-push-microservices:
  # Only running this in main as it is assumed the tests above test within the container (thus
  # testing the container can be built).
  <<: *rules-main-only
  stage: microservice-build-push
  image: docker:20.10.8
  services:
    - docker:20.10.8-dind
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
  before_script:
    - apk add make bash git python3
    - *restore-mtime
  script:
    # Combining the build and push as the image is not available if they
    # are in separate jobs.
    - make docker.login
    - DOCKER_BUILD_ARGS="--build-arg BUILD_TYPE=$BUILD_TYPE --build-arg PIP_ARGS='$PIP_ARGS'" make images
    - make images.push

# TODO: docker image not carried over from build
#   combine build/push (only do push on master change)
#   save images as artifacts and load them in push rule
#push-microservices:
#  <<: *rules-main-only
#  stage: microservice-push
#  image: docker:20.10.8
#  services:
#    - docker:20.10.8-dind
#  variables:
#    DOCKER_TLS_CERTDIR: "/certs"
#  before_script:
#    - apk add make bash git python3
#    - *restore-mtime
#  script:
#    - make docker.login
#    - make images.push

# TODO: hold off on any scanning yet until we have more information
# from security about what that involves, but it will almost surely
# involve more than what is built into the Makefile currently.
# TODO: Once above TODO is figured out, see if it should happen on MR
#scan-microservices:
#  stage: microservice-scan
#  script:
#    - make images.scan

# TODO: this makes Argo CD stuff difficult. Images built on the higher sides
# won't necessarily have the same tag (commit ID) as on the low side since
# this will only be run for the last commit after an upscan, even though an
# image on the low side will have built built for a previous commit.
# So if we upscan the Argo CD repo then the image tags coming up won't match.
# But if this is run then it'll make commits which are overridden by the next
# upscan. If we don't upscan the Argo CD apps, then we need to make duplicates
# on each network.
# TODO: uncomment (don't want to set this up in gitlab.com to clone/push repo)
#deploy-microservices:
#  <<: *rules-main-only
#  stage: deploy
#  image: python:3.9.6-buster
#  before_script:
#    - *restore-mtime
#  script:
#    - make argo-cd.update-apps
